<?php

class paystack_controller extends HBController {

    /**
     *
     * @var iyzipay
     */
    var $module;

    /**
     *
     * @var UserAuthorization
     */
    var $authorization;

    /**
     * ?cmd=rave&action=pay&txref=HB_1_1_1111
     * @param <type> $params
     */


    public function pay($params) {
        if($client_id = $this->authorization->get_id())
            $params['client_id'] = $client_id;
        if($this->module->capture_token($params))
            Utilities::redirect('?cmd=clientarea&action=invoice&id='.$params['invoice_id']);
        else
            Utilities::redirect('?cmd=clientarea&action=invoices');
 }

}