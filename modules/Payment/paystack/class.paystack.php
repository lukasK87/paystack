<?php
require_once(__DIR__.'/vendor/autoload.php');

/**
 * HostBill example gateway module
 * @see http://dev.hostbillapp.com/dev-kit/payment-gateway-modules/
 *
 * @author HostBill Module Generator <cs@hostbillapp.com>
 */

class paystack extends PaymentModule   {

    /**
     * List of currencies supported by gateway - if module supports all currencies - leave empty
     * @var array
     */
    protected $supportedCurrencies = array('NGN');


    /**
     * Module version. Make sure to increase version any time you add some
     * new functions in this class!
     * @var string
     */
    protected $version = '1.1';

    /**
     * Module name, visible in admin portal.
     * @var string
     */
    protected $modname = 'Paystack.co';

    /**
     * Module description, visible in admin portal
     * @var string
     */
    protected $description = 'Integration with paystack.co';



    /**
     * Configuration array
     */
    protected $configuration = [
        'Public Key' => [
            'value' => '', //this will auto-load from db
            'description' => '',
            'type' => 'input',
            'default' => '',


        ],
        'Secret Key' => [
            'value' => '', //this will auto-load from db
            'description' => '',
            'type' => 'input',
            'default' => '',


        ]
    ];



    public function __construct() {
        parent::__construct();

    }

    /**
     * Return HTML code that should be displayed in clientarea for client to pay (ie. in invoice details)
     * @return string
     */
    public function drawForm()
    {

        $ccard = HBLoader::LoadModel('Clients/CreditCardsUtils')->getCreditCard($this->client['id'], true, $this->id);


        if (!$ccard) {

            $url = $this->generatePaymentUrl();


            if (!$url) {
                $this->logActivity([
                    'output' => ['error' => 'Cannot create paystack redirect url'],
                    'result' => self::PAYMENT_FAILURE
                ]);
                $this->addError("Payment failed please contact administrator");

            } else {
                $form = '<form action="' . $url . '" method="post" name="payform">';
                $form .= '<input type="submit" value="' . $this->paynow_text() . '" />';
                $form .= '</form>';


            }
        }
        else{

            $security_token = Tokenizer::getSalt();
            $url = Utilities::url('?cmd=paystack&action=pay');

            $form = '<form action="' . $url . '" method="post" name="payform">';
            $form .='<input type="hidden" name="security_token" value="'.$security_token.'">';
            $form .='<input type="hidden" name="invoice_id" value="'.$this->invoice_id.'">';
            $form .= '<input type="submit" value="' . $this->paynow_text() . '" />';
            $form .= '</form>';
        }

        return $form;
     }


    public function generatePaymentUrl(){

        $params = [
            'reference'     => $this->invoice_id.'--'.$this->client['id'].'--'.time(),
            'callback_url'  => 'https://dpay.hostbill.com?cmd=paystack&action=pay&invoice_id='.$this->invoice_id,
            'amount'        => (string)($this->amount * 100),
            'email'         => $this->client['email'],

        ];
        $payment = new Yabacon\Paystack($this->configuration['Secret Key']['value']);


        $payment_call = $payment->transaction->initialize($params);



        if($payment_call->status){
            return $payment_call->data->authorization_url;

        }
        else
            return false;

    }

    public function capture_token($params){

        if(isset($params['trxref'])) {
            $verify = new Yabacon\Paystack($this->configuration['Secret Key']['value']);
            try {
                // verify using the library
                $tranx = $verify->transaction->verify([
                    'reference' => $params['trxref'],
                ]);
            } catch (\Yabacon\Paystack\Exception\ApiException $e) {

                $this->logActivity(array(
                    'result' => PaymentModule::PAYMENT_FAILURE,
                    'output' => ['error' => 'Payment error: ' . $e->getMessage()]));
                return null;

            }

            if ('success' === $tranx->data->status) {

                $infos = explode('--', $tranx->data->reference);
                if (!$this->_transactionExists($params['trxref'])) {


                    $amount = floatval($tranx->data->amount) / 100;
                    $this->addTransaction(array(
                        'in' => $amount,
                        'invoice_id' => $infos[0],
                        'transaction_id' => $params['trxref']
                    ));
                }

                if (!HBLoader::LoadModel('Clients/CreditCardsUtils')->getCreditCard($this->client['id'], true, $this->id)) {
                    HBLoader::LoadModel('Clients/CreditCardsUtils')->TokenizeNotStoredCC($infos[1], $tranx->data->authorization->last4, $tranx->data->authorization->authorization_code, $this->id);
                }

            }

            $this->logActivity(array(
                'result' => PaymentModule::PAYMENT_FAILURE,
                'output' => ['error' => 'Payment ststus error: ' . $tranx->data->status]));
            return null;
        }
        else{
            if(!$params['token_valid'])
            {
                $this->logActivity(array(
                    'result' => PaymentModule::PAYMENT_FAILURE,
                    'output' => ['error' => 'Invalid token ']));
                return null;
            }

            $this->collect($params);

        }


    }

    public function collect($params){



        $invoice = Invoice::createInvoice($params['invoice_id']);
        $client_id = $params['client_id'];

        if ( $invoice->getInvoiceId() != $params['invoice_id'] || $invoice->getClientId() != $client_id) {
            $this->logActivity(array(
                'result' => PaymentModule::PAYMENT_FAILURE,
                'output' => ['error' => 'Invalid invoice_id ']));
            return false;

        }

        $clients = HBLoader::LoadModel('Clients');
        $client_details = $clients->getClient($client_id);


        $ccard = HBLoader::LoadModel('Clients/CreditCardsUtils')->getCreditCard($client_id, true, $this->id);

        $reference = $params['invoice_id'].'--'.$client_id.'--'.time();
        $postdata =  [
            'authorization_code' => $ccard['token'],
            'email' => $client_details['email'],
            'amount' => (string)($invoice->getTotal()*100),
            "reference" => $reference,
        ];


        $url = "https://api.paystack.co/transaction/charge_authorization";

        $request = $this->request($url, $postdata);

        if($request){
            $result = json_decode($request, true);
            if($result['status']!='success'){
                $this->logActivity(array(
                    'result' => PaymentModule::PAYMENT_FAILURE,
                    'output' => ['error' => 'Wrong payment status: '.$result]));
                return false;
            }

            if (!$this->_transactionExists($params['trxref'])) {


                $payed = $this->addTransaction(array(
                    'in' => $invoice->getTotal(),
                    'invoice_id' => $params['invoice_id'],
                    'transaction_id' => $reference
                ));
                $this->logActivity(array(
                    'result' => PaymentModule::PAYMENT_SUCCESS,
                    'output' => ['error' => 'Payment ok: '.$payed]));
                return $payed;
            }

            $this->logActivity(array(
                'result' => PaymentModule::PAYMENT_SUCCESS,
                'output' => ['error' => 'Invoice already paid: '.$result]));
            return true;
        }

        $this->logActivity(array(
            'result' => PaymentModule::PAYMENT_FAILURE,
            'output' => ['error' => 'Payment error ']));
        return false;

    }

    /**
     * Handle payment refund
     *
     * @param $_transaction
     */
    public function refund($_transaction, $amount) {


        $postdata = [
            'transaction'   =>  $_transaction,
            'amount'        =>  (string)($amount * 100),

        ];

        $url = "https://api.paystack.co/refund";

        $request = $this->request($url, $postdata);

        if($request){
            $response = json_decode($request, 1);
            if ($response['status']) {
                $result = $this->addRefund(array(
                    'target_transaction_number' => $_transaction,
                    'transaction_id' => $response['data']['transaction']['id'],
                    'amount' => $amount
                ));

                $this->logActivity(array(
                    'output' => (array)$response,
                    'result' => $result ? self::PAYMENT_SUCCESS : self::PAYMENT_FAILURE
                ));
                return $result;

            }
            else{
                $this->logActivity(array(
                    'output' => ['error' => $request],
                        'result' => self::PAYMENT_FAILURE
                ));

                return false;
            }
        }

        $this->logActivity(array(
                'output' => ['error' => 'Refund request connection error'],
                'result' => self::PAYMENT_FAILURE
                ));

        return false;
     }

    public function request($url, $data){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
            'Authorization: Bearer '.$this->configuration['Secret Key']['value'],
            'Content-Type: application/json',
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $request = curl_exec ($ch);
        $error = curl_error($ch);
        curl_close ($ch);

        if($error){
            $this->logActivity(array(
                'result' => PaymentModule::PAYMENT_FAILURE,
                'output' => ['error' => 'Connection error: '.$error]));
            return false;
        }

        return $request;
    }








}